This module saves a author information entered into a node's field as the node author. If you are using Feeds, you can map your author information from the feed into this field.

Install this module and configure it at admin/content/feeds_user_map.

This module is very simple. It looks for field you have created on a content type. It looks up that field's content and sees if there is a Drupal user that matches that string. If so, it saves the corresponding Drupal author for that node. It does this any time the node is updated or saved. If no user exists, then an email is sent to the site administrator with reasonably client-friendly instructions about what to do.

Note: YOU MUST enter the machine name of your content type and your field.

I wrote this because Feeds module cannot save node authors from imported feeds as Drupal users. Its hooks don't have access to the node object. 

To use this module, create a field on a content type. Create a feed that writes author information to that field. Then go to admin/content/feeds_user_map and enter your content type machine name and your field machine name and your users will be saved on import.

If you want multiple content types and fields, better usability, D7 release, etc. create an issue. It wouldn't be too hard but I'm on deadline right now and I just wanted to release this in case it was useful to someone else.